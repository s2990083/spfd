document.querySelectorAll('button.theme-selection').forEach(button => {
  button.addEventListener('pointerdown', e => {
    setTheme(e.target.dataset.theme);
    localStorage.setItem('theme', e.target.dataset.theme)
  })
})

fetch("/api/me").then(res => {
  if (res.ok) {
    res.json().then(json => {
      document.getElementById("username").innerHTML = escapeHTML(json.username);
      document.getElementById("theme").innerHTML = json.setting_theme;
      if (json.permission_level == 0) {
        document.getElementById("admin_link").setAttribute("style", "visibility: visible;")
      }
    })
  }
})
.catch(console.log);


async function logout() {
  await fetch('../api/session/logout', {method:"DELETE"});
  document.cookie = 'session=; expires= Thu, 1 Jan 1970 00:00:00 UTC; path=/;'
  location.reload();
}