const deleteUserPopup = document.getElementById("deleteUserPopup")

document.querySelectorAll('button.theme-selection').forEach(button => {
  button.addEventListener('pointerdown', e => {
    setTheme(e.target.dataset.theme);
    localStorage.setItem('theme', e.target.dataset.theme)
  })
})

fetch("../api/me").then(res => {
  if (res.ok) {
    res.json().then(json => {
      // document.getElementById("username").innerHTML = json.username;
      // document.getElementById("theme").innerHTML = json.setting_theme;
    })
  }
})
.catch(console.log);


function editUser(uid) {
  fetch('../api/user/' + uid, {method:"put"}).then(res => {location.reload();}).catch(err => alert("Some error occured"));
}

function deleteUser(uid) {
  fetch('../api/user/' + uid, {method:"delete"}).then(res => {location.reload();}).catch(err => alert("Some error occurred"));
}

document.getElementById('delete-cancel-user').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteUserPopup.close();
})

document.getElementById('delete-confirm-user').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteUser(deleteUserPopup.dataset.userId)
})

document.querySelector('.cancel').addEventListener('pointerdown', e => {
    e.preventDefault();
    window.location.href = '../admin';
})