const dashContainer = document.getElementById('articles');

getDashboards();

function getDashboards() {
  // get all the dashboards
  var myHeaders = new Headers();
  myHeaders.append("Accept", "application/json");

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  fetch("../api/dashboards", requestOptions)
    .then(response => response.json())
    .then(result => {
      result.forEach(dashboard => {
        showDashboard(dashboard);
      })
    })
    .catch(error => console.error(error));
}

function showDashboard(dashboard) {
  let a = document.createElement("a");
  a.setAttribute("href", `/dashboard?id=${dashboard.id}`);
  let name = document.createElement("h2");
  name.textContent = escapeHTML(dashboard.name);
  let br = document.createElement("br");
  let icon = document.createElement("span");
  icon.classList.add("material-icons-round");
  icon.textContent = dashboard.icon;
  a.appendChild(name);
  a.appendChild(br);
  a.appendChild(icon);
  dashContainer.appendChild(a);
}