const deleteUserPopup = document.getElementById("deleteUserPopup")

document.querySelectorAll('button.theme-selection').forEach(button => {
  button.addEventListener('pointerdown', e => {
    setTheme(e.target.dataset.theme);
    localStorage.setItem('theme', e.target.dataset.theme)
  })
})

function getUsers() {
  fetch('../api/users/all').then(res => {
    if (res.ok) {
      res.json().then(json => {
        let html = "No users found"
        if (json.length > 0) {
          html = "<table> <tr><th>Username</th><th>Permission level</th><th>Edit permissions</th><th>Delete</th></tr>";

          for (let i = 0; i < json.length; i++) {
            html += "<tr>";
            html += "<td>" + escapeHTML(json[i].username) + "</td>";
            if (json[i].permission_level == 0) {
              html += "<td>Admin</td>";
              html += `<td><button onclick="editUser(${json[i].uid})">Demote User</button></td>`;
            } else if (json[i].permission_level == 1) {
              html += "<td>Normal User</td>";
              html += `<td><button onclick="editUser(${json[i].uid})">Promote User</button></td>`;
            }
            //html += "<td>" + json[i].permission_level + "</td>";

            html += `<td><button class="cancel delete-user" data-user="${json[i].uid}" data-username="${json[i].username}">Delete User</button></td>`;
            html += "</tr>";
          }
          html += "</table>";
        }
        document.getElementById("usersTable").innerHTML = html;

        document.querySelectorAll('.delete-user').forEach(button => {
          button.addEventListener('pointerdown', e => {
              e.preventDefault()
              if (e.button !== 0) {return}
              console.log(e);
              document.getElementById("userName").innerHTML = escapeHTML(e.target.dataset.username);
              deleteUserPopup.dataset.userId = e.target.dataset.user;
              deleteUserPopup.showModal();
          })
        })
      })
    }
    }).catch(console.log);

}

function editUser(uid) {
  fetch('../api/user/' + uid, {method:"put"}).then(res => {location.reload();}).catch(err => alert("Some error occured"));
}

function deleteUser(uid) {
  fetch('../api/user/' + uid, {method:"delete"}).then(res => {location.reload();}).catch(err => alert("Some error occurred"));
}

document.getElementById('delete-cancel-user').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteUserPopup.close();
})
document.getElementById('delete-confirm-user').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteUser(deleteUserPopup.dataset.userId)
})