const form = document.querySelector("form")
form.addEventListener("submit", confirm)
const draggableContainer = document.getElementById("draggables")
getTables();
getDashboards();
const previewCanvas = document.querySelector('canvas#preview');
const previewButton = document.getElementById('load-preview');
let chart;

previewButton.addEventListener('pointerdown', previewChart);

function confirm(e) {
  e.preventDefault();

  let requestOptions = getInput();

  fetch("../api/charts", requestOptions)
    .then(response => {
        if (response.status == 400) {
            alert("You do not have access to that dashboard")
        } else if (!response.ok) {
            alert("Something went wrong. Maybe your configuration is invalid?")
            console.error(response)
        } else {
            window.location = `../dashboard?id=${requestOptions.body.get("dash_id")}`;
        }
    })
    .catch(error => console.log('error', error));
}

function getTables() {
  let myHeaders = new Headers();
  myHeaders.append("Accept", "application/json");

  let requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  fetch("../api/data/tables", requestOptions)
    .then(response => response.json())
    .then(json => {
      json.forEach(table => {
        let tableDiv = document.createElement('div');
        tableDiv.classList.add('table-group');
        tableDiv.dataset.table = table.tablename;
        let tableHeader = document.createElement('button');
        tableHeader.textContent = table.tablename;
        tableHeader.classList.add('table-group-header');
        tableDiv.appendChild(tableHeader);
        let columnContainer = document.createElement('div');
        columnContainer.classList.add('column-container')
        tableDiv.appendChild(columnContainer);
        draggableContainer.appendChild(tableDiv);
        table.columns.forEach(column => {
          let columnDiv = document.createElement('div')
          // <div id="drag-item-1" class="drag-item" draggable="true"><text>AFTS_TimeSheet</text></div>
          columnDiv.classList.add('drag-item')
          columnDiv.id = `draggable-${table.tablename}-${column.columnName}`
          columnDiv.setAttribute('draggable', true)
          columnDiv.dataset.table = table.tablename;
          columnDiv.dataset.column = column.columnName;
          columnDiv.dataset.type = column.type;
          let text = document.createElement('text');
          text.textContent = column.columnName;
          // text.appendChild(document.createTextNode(column.columnName));
          columnDiv.appendChild(text);
          columnContainer.appendChild(columnDiv);
          // console.log(`${table.tablename}.${column.columnName}::${column.type}`)
        })
      })
      document.querySelectorAll('.drop-field').forEach(dropField => {
        dropField.addEventListener('drop', drop);
        dropField.addEventListener('dragover', allowDrop);
      });
      document.querySelectorAll('.drag-item').forEach(dragItem => {
        dragItem.addEventListener('dragstart', drag)
      });
      let tableGroupHeaders = [...document.querySelectorAll(".table-group-header")];
        for (let i = 0; i < tableGroupHeaders.length; i++) {
            tableGroupHeaders[i].addEventListener("pointerdown", (e) => {
            e.target.classList.toggle("active");
            let columnContainer = e.target.nextElementSibling;
            if (columnContainer.style.maxHeight) {
              columnContainer.style.maxHeight = null;
            } else {
              columnContainer.style.maxHeight = columnContainer.scrollHeight + "px";
            }
          });
        }
    })
    .catch(error => console.log('error', error));
}

function getDashboards() {
  // get all the dashboards
  let myHeaders = new Headers();
  myHeaders.append("Accept", "application/json");

  let requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  const dashSelection = document.getElementById('dash_id');

  fetch("../api/dashboards?owned=true", requestOptions)
    .then(response => response.json())
    .then(result => {
      result.forEach(dashboard => {
        let option = document.createElement('option')
        option.setAttribute('value', dashboard.id);
        option.textContent = dashboard.name;
        dashSelection.appendChild(option);
      })
      let dash = parseInt(new URLSearchParams(window.location.search).get('dash'))
      if (!isNaN(dash)) {
        dashSelection.value = dash;
      }
    })
    .catch(error => console.error(error));
}

function allowDrop(ev) {
  // ev.target.innerHTML = "";
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  let data = ev.dataTransfer.getData("text");
  let dropField = ev.target;
  while (!dropField.classList.contains('drop-field')) {
    dropField = dropField.parentNode;
  }
  while (dropField.childNodes[0]) {
    dropField.removeChild(dropField.childNodes[0]);
  }
  dropField.appendChild(document.getElementById(data).cloneNode(true));
  let droppedItem = dropField.childNodes[0]
  droppedItem.querySelector('text').textContent = `${droppedItem.dataset.table}.${droppedItem.dataset.column}`;

  let disable = []
  switch(droppedItem.dataset.type) {
      case 'text':
        disable = ['SUM', 'AVG'];
        break;
      case 'date':
        disable = ['SUM', 'AVG'];
        break;
      case 'boolean':
        disable = ['SUM', 'AVG', 'MIN', 'MAX'];
        break;
      default:
        disable = [];
        break;
  }
  dropField.parentNode.parentNode.querySelectorAll('option').forEach(option => {
    if(disable.includes(option.getAttribute('value'))) {
      option.setAttribute('disabled', 'true');
    } else {
      option.removeAttribute('disabled');
    }
  })
}

function removeDroppedField(field) {
  document.getElementById(field).innerHTML = "";
}

function getInput() {
  const data = new FormData(form);
  const title = data.get('title');
  const type = data.get('type');
  const rowlimit = data.get('rowlimit');
  const dashId = data.get('dash_id');
  const tables = [...document.querySelectorAll(".drop-field .drag-item")].map(item => item.dataset.table);
  const columns = [...document.querySelectorAll(".drop-field .drag-item")].map(item => item.dataset.column);
  const aggregates = [...document.querySelectorAll("select")].filter(item => item.id.startsWith("aggregate")).map(item => item.value);
  const labels = [...document.querySelectorAll("input")].filter(item => item.id.startsWith("label")).map(item => item.value);
  const columnCount = Math.min(tables.filter(value => {
        return (value !== '' && value !== 'null' && value !== null)
      }).length,
      columns.filter(value => {
        return (value !== '' && value !== 'null' && value !== null)
      }).length,
      labels.filter(value => {
        return (value !== '' && value !== 'null' && value !== null)
      }).length);
  let tableList = tables[0];
  let columnList = columns[0];
  let usageList = 'labels';
  let aggregateList = 'undefined';
  let labelList = labels[0];
  for (let i = 1; i < columnCount; i++) {
    tableList += `,${tables[i]}`;
    columnList += `,${columns[i]}`;
    usageList += ',values';
    aggregateList += `,${aggregates[i-1] === '' ? 'undefined' : aggregates[i-1]}`;
    labelList += `,${labels[i]}`;
  }

  let myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
  myHeaders.append("Accept", "application/json");

  let urlencoded = new URLSearchParams();
  urlencoded.append("title", title);
  urlencoded.append("type", type);
  urlencoded.append("rowlimit", rowlimit);
  urlencoded.append("dash_id", dashId);
  urlencoded.append("tables", tableList);
  urlencoded.append("columns", columnList);
  urlencoded.append("usages", usageList);
  urlencoded.append("aggregates", aggregateList);
  urlencoded.append("labels", labelList);

  return {
    method: 'POST',
    headers: myHeaders,
    body: urlencoded,
    redirect: 'follow'
  };
}

function previewChart() {
  let requestOptions = getInput();

  fetch("../api/charts/preview", requestOptions)
    .then(response => response.json())
    .then(json => {
      const datasets = json.columns.filter(column => {return column.usage === 'values'})
        .map(column => {return {label: column.label, data: column.data}});
      if(chart) chart.destroy();
      chart = new Chart(previewCanvas, {
          type: json.type,
          data: {
              labels: json.columns.find(column => {
                  return column.usage === 'labels'
              }).data,
              datasets
          },
          options: {
              plugins: {
                  title: {
                      display: true,
                      text: json.title
                  }
              }
          }
      })
    })
    .catch(error => console.log('error', error));
}

function addDataset() {
    const datasetContainer = document.getElementById('datasets')
    const id = Math.max(...[...datasetContainer.querySelectorAll('fieldset')].map(fieldset => parseInt(fieldset.dataset.dataset))) + 1

    const fieldset = document.createElement('fieldset')
    fieldset.dataset.dataset = id;
    fieldset.innerHTML =
    `<legend>dataset ${id}</legend>
      <text>Column:</text><br>
      <div class="drop-field-box">
        <div id="drop-field-${id+1}" class="drop-field"></div>
        <span class="material-icons-round" onclick="removeDroppedField('drop-field-${id+1}')">close</span>
      </div>
      <label for="aggregate${id+1}">Aggregate:</label><br>
      <select id="aggregate${id+1}" name="aggregate${id+1}">
        <option value="">None</option>
        <option value="SUM">Sum</option>
        <option value="COUNT">Count</option>
        <option value="AVG">Average</option>
        <option value="MIN">Minimum</option>
        <option value="MAX">Maximum</option>
      </select>
      <label for="label${id+1}">Label:</label><br>
      <input type="text" id="label${id+1}" name="label${id+1}" placeholder="Label ${id}">`

    datasetContainer.insertBefore(fieldset, document.getElementById('dataset-buttons'))

    fieldset.querySelector('.drop-field').addEventListener('drop', drop);
    fieldset.querySelector('.drop-field').addEventListener('dragover', allowDrop);
}

function removeDataset() {
    const datasetContainer = document.getElementById('datasets')
    let nodes = [...datasetContainer.querySelectorAll('fieldset')]
    if (nodes.length <= 1) return;
    let newest = Math.max(...nodes.map(fieldset => parseInt(fieldset.dataset.dataset)))
    let node = nodes.find(node => node.dataset.dataset == newest);
    datasetContainer.removeChild(node);
}

document.getElementById('remove-dataset').addEventListener('pointerdown', e => {
  e.preventDefault()
  if (e.button !== 0) {return}
  removeDataset()
})

document.getElementById('add-dataset').addEventListener('pointerdown', e => {
  e.preventDefault()
  if (e.button !== 0) {return}
  addDataset()
})