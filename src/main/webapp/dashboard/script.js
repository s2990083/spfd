const chartContainer = document.getElementById('chartContainer');
const addChart = document.getElementById('add-chart')
const deleteDashboard = document.getElementById('delete-dashboard')
const dashId = parseInt(new URLSearchParams(window.location.search).get('id'));
const deleteDashboardPopup = document.getElementById('deleteDashboardPopup');
const deleteChartPopup = document.getElementById('deleteChartPopup');


getDashboardContents();

function getDashboardContents() {
  // get the charts on this dashboard
  var myHeaders = new Headers();
  myHeaders.append("Accept", "application/json");

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  fetch(`../api/dashboards/${dashId}`, requestOptions)
    .then(response => response.json())
    .then(json => {
      title.innerHTML = json.name;

      Promise.all([userId]).then(userId => {
        let isOwner = (userId[0] === json.owner)

        if (!isOwner) {
          chartContainer.removeChild(document.getElementById('add-chart'))
          chartContainer.removeChild(document.getElementById('delete-dashboard'))
        }

        json.charts.forEach(chart => {
            generateChart(chart, isOwner)
          })
      })
    })
    .catch(error => console.error(error));
}

function generateChart(json, deletable) {
    let chartDiv = document.createElement("div");
    chartDiv.classList.add("chart")
    chartDiv.classList.add("overlay")
    let title = document.createElement("h2")
    title.textContent = escapeHTML(json.title);
    chartDiv.appendChild(title);
    if (deletable) {
        let deleteButton = document.createElement("button");
        deleteButton.classList.add("cancel");
        deleteButton.classList.add("delete-chart-button");
        chartDiv.appendChild(deleteButton);
        let span = document.createElement("span");
        span.classList.add("material-icons-round");
        span.textContent = "delete";
        deleteButton.appendChild(span);

        deleteButton.addEventListener('pointerdown', e => {
            e.preventDefault()
            if (e.button !== 0) {
                return
            }
            console.log(e);
            deleteChartPopup.dataset.chartId = json.id;
            deleteChartPopup.showModal();
        })
    }
    let canvasContainer = document.createElement("div");
    canvasContainer.classList.add('canvas-container')
    chartDiv.appendChild(canvasContainer);
    let canvas = document.createElement("canvas");
    canvasContainer.appendChild(canvas);
    if (document.getElementById('add-chart')) {
        chartContainer.insertBefore(chartDiv, addChart);
    } else {
        chartContainer.appendChild(chartDiv)
    }
    const datasets = json.columns.filter(column => {
        return column.usage === 'values'
    })
        .map(column => {
            return {label: escapeHTML(column.label), data: column.data}
        })

    new Chart(canvas, {
        type: json.type,
        data: {
            labels: json.columns.find(column => {
                return column.usage === 'labels'
            }).data,
            datasets
        },
    })
}

deleteDashboard.addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteDashboardPopup.showModal();
})
document.getElementById('delete-cancel-dashboard').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  console.log(e)
  if (e.button !== 0) {return}
  deleteDashboardPopup.close();
})
document.getElementById('delete-confirm-dashboard').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  confirmDelete();
})

function confirmDelete() {
  let requestOptions = {
    method: 'DELETE',
    redirect: 'follow'
  };

  fetch(`../api/dashboards/${dashId}`, requestOptions)
    .then(response => response.text())
    .then(result => window.location = "../dashboard-home")
    .catch(error => console.log('error', error));
}

document.querySelectorAll('.delete-chart-button').forEach(button => {
    button.addEventListener('pointerdown', e => {
          e.preventDefault()
          if (e.button !== 0) {return}
          console.log(e);
          // deleteChartPopup.dataset.chartId =
          deleteChartPopup.showModal();
    })
})

document.getElementById('add-chart').setAttribute('href', `../create-chart?dash=${dashId}`)

document.getElementById('delete-cancel-chart').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  deleteChartPopup.close();
})
document.getElementById('delete-confirm-chart').addEventListener('pointerdown', (e) => {
  e.preventDefault()
  if (e.button !== 0) {return}
  confirmDeleteChart(deleteChartPopup.dataset.chartId)
})

function confirmDeleteChart(id) {
  let requestOptions = {
    method: 'DELETE',
    redirect: 'follow'
  };

  fetch(`../api/charts/${id}`, requestOptions)
    .then(response => response.text())
    .then(result => location.reload())
    .catch(error => console.log('error', error));
}

let userId = new Promise((resolve, reject) => {
     fetch("/api/me").then(res => {
      if (res.ok) {
        res.json().then(json => resolve(json.uid))
      }
    })
})
