const logo = document.querySelector('#header-logo picture');
getSelectedTheme();

function setTheme(theme) {
      document.documentElement.classList.toggle("color-platinum", theme === 'platinum')
      document.documentElement.classList.toggle("color-night", theme === 'night')
      document.documentElement.classList.toggle("color-elements", theme === 'elements')
      document.documentElement.classList.toggle("color-business", theme === 'business')
      if(theme === 'night' || theme === 'elements') {
            logo.querySelector('img').setAttribute('src', '../media/af-logo-light.svg');
            logo.querySelector('source').setAttribute('srcset', '../media/af-logo-light.svg');
      } else if(theme === 'platinum' || theme === 'business') {
            logo.querySelector('img').setAttribute('src', '../media/af-logo-dark.svg');
            logo.querySelector('source').setAttribute('srcset', '../media/af-logo-dark.svg');
      }
}

function getSelectedTheme() {
  const theme = localStorage.getItem('theme')
  if (theme === null) return;
  setTheme(theme);
}

function escapeHTML(str){
    return new Option(str).innerHTML;
}