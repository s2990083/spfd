package org.spfd;

import java.sql.*;


public class DBConnector {

//    static String host = "bronto.ewi.utwente.nl";
//    static String dbName = "dab_di22232b_30";
//    static String url = "jdbc:postgresql://" + host + ":5432/" + dbName + "?currentSchema=ActFact";
//    static String username = "dab_di22232b_30";
//    static String password = "aiQ0WPRhMxjGu7bS";
    static String host = "10.34.10.228";
    static String dbName = "postgres";
    static String url = "jdbc:postgresql://" + host + ":5432/" + dbName + "?currentSchema=ActFact";
    static String username = "webadmin";
    static String password = "sprint";

    private static Connection connection = null;

    public static Connection connect() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Error loading driver: " + cnfe);
        }
        return DriverManager.getConnection(url, username, password);
    }

    public static ResultSet doQuery(String q) {
        try (Connection connection = connect()) {
            Statement st = connection.createStatement();
            return st.executeQuery(q);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void doUpdate(String q) {
        try (Connection connection = connect()) {
            Statement st = connection.createStatement();
            st.executeUpdate(q);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
