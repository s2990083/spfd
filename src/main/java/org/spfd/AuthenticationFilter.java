package org.spfd;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.spfd.data.User;
import org.spfd.resources.MeResource;

import java.io.IOException;

public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request_, ServletResponse response_, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) request_;
        HttpServletResponse response = (HttpServletResponse) response_;
        User user = MeResource.get_user(request);

        // If this is a login request
        if (request.getRequestURI().startsWith("/api/session/login") || request.getRequestURI().startsWith("/login") || request.getRequestURI().startsWith("/assets") || request.getRequestURI().startsWith("/media"))   {
            chain.doFilter(request, response);
            return;
        }

        if (user == null) {
            response.sendRedirect("/login");
        } else {
            request.setAttribute("user", user);
            chain.doFilter(request, response);
        }

    }
}
