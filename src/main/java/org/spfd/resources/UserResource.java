package org.spfd.resources;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.JAXBElement;
import org.mindrot.jbcrypt.BCrypt;
import org.spfd.DBConnector;
import org.spfd.data.Session;
import org.spfd.data.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@Path("user")
public class UserResource {

    @GET
    @Produces({MediaType.APPLICATION_XML})
    public User getUser(){
        return new User(1,"name","pass",1,"dark");
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createUser(@Context HttpServletRequest request,
                        @Context HttpServletResponse response,
                        @FormParam("username") String username,
                        @FormParam("password") String password,
                        @FormParam("admin") String admin) throws IOException {

        String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
        User user = (User) request.getAttribute("user");

        // TODO do something with admin

        int permission_level = 1;
        if (admin != null && admin.equals("on")) {
            permission_level = 0;
        }

        if (user.permission_level == 0) {
            User.saveUser(username, hashed, permission_level);
            response.sendRedirect("/admin");
        } else {
            response.setStatus(400);
        }
    }

    @DELETE
    @Path("{uid}")
    public void removeUser(@Context HttpServletRequest request,
                           @Context HttpServletResponse response,
                           @PathParam("uid") String uid) {
        User user = (User) request.getAttribute("user");

        if (user.permission_level == 0) { //user that did the request is admin
            User.removeUser(Integer.parseInt(uid));
            response.setStatus(200);
        } else {
            response.setStatus(400);
        }
    }

    @PUT
    @Path("{uid}")
    public void editUser(@Context HttpServletRequest request,
                           @Context HttpServletResponse response,
                           @PathParam("uid") String uid) {
        User user = (User) request.getAttribute("user");


        if (user.permission_level == 0) {       //user that did the request is admin
            System.out.println("updating user permission");
            if (User.getPermission_level(Integer.parseInt(uid)) == 0) {
                User.demoteUser(Integer.parseInt(uid));
            } else if (User.getPermission_level(Integer.parseInt(uid)) == 1) {
                User.promoteUser(Integer.parseInt(uid));
            }

            response.setStatus(200);
        } else {
            response.setStatus(400);
        }
    }

}
