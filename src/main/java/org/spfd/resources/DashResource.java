package org.spfd.resources;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import org.spfd.DBConnector;
import org.spfd.data.Dashboard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class DashResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    int id;

    public DashResource(UriInfo uriInfo, Request request, int id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Dashboard getDash() throws SQLException {
        String query = "SELECT dashboard.dash_id, name, owner, icon, array_agg(graph_id) AS graphs " +
                "FROM userdata.dashboard " +
            "LEFT JOIN userdata.has_graph ON dashboard.dash_id = has_graph.dash_id " +
            "WHERE dashboard.dash_id = ?" +
            " GROUP BY dashboard.dash_id, name, owner, icon;";
        Connection conn = DBConnector.connect();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if(rs.next()) {
            int id = rs.getInt("dash_id");
            String name = rs.getString("name");
            int owner = rs.getInt("owner");
            String icon = rs.getString("icon");
            Object[] graphIds = (Object[]) rs.getArray("graphs").getArray();
            return new Dashboard(id, name, icon, graphIds, owner);
        }
        return null;
    }

    @DELETE
    public void deleteDash() {
        String query = "DELETE FROM userdata.graphs " +
                "WHERE graph_id IN (" +
                "SELECT graph_id " +
                "FROM userdata.has_graph " +
                "WHERE dash_id = " + id +
                ");";
        DBConnector.doUpdate(query);

        query = "DELETE FROM userdata.dashboard " +
                "WHERE dash_id = " + id;
        DBConnector.doUpdate(query);
    }
}
