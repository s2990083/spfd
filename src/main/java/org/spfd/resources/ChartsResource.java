package org.spfd.resources;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import org.spfd.DBConnector;
import org.spfd.data.Chart;
import org.spfd.data.User;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Path("charts")
public class ChartsResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Chart> getCharts() {
        List<Chart> graphs = new ArrayList<>();
        String query = "SELECT graphs.graph_id, title, type, owner, rowlimit, array_agg(data_table) AS tables, array_agg(data_column) AS columns, array_agg(USAGE) AS usages, array_agg(AGGREGATE) AS aggregates, array_agg(label) AS labels " +
                "FROM userdata.graphs " +
                "JOIN userdata.columns ON graphs.graph_id = columns.graph_id " +
                "GROUP BY graphs.graph_id, title, type, owner, rowlimit; ";
        try (ResultSet rs = DBConnector.doQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("graph_id");
                String title = rs.getString("title");
                String type = rs.getString("type");
                int owner = rs.getInt("owner");
                int limit = rs.getInt("rowlimit");
                String[] tables = (String[]) rs.getArray("tables").getArray();
                String[] columns = (String[]) rs.getArray("columns").getArray();
                String[] usages = (String[]) rs.getArray("usages").getArray();
                String[] aggregates = (String[]) rs.getArray("aggregates").getArray();
                String[] labels = (String[]) rs.getArray("labels").getArray();
                graphs.add(new Chart(id, title, type, owner, limit, tables, columns, usages, aggregates, labels));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return graphs;
    }

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public void createChart(@FormParam("title") String title,
                           @FormParam("type") String type,
                           @FormParam("rowlimit") int rowlimit,
                           @FormParam("dash_id") int dash_id,
                           @FormParam("tables") String tables,
                           @FormParam("columns") String columns,
                           @FormParam("usages") String usages,
                           @FormParam("aggregates") String aggregates,
                           @FormParam("labels") String labels,
                            @Context HttpServletResponse response,
                            @Context HttpServletRequest request) throws SQLException, IOException {
        int owner = ((User) request.getAttribute("user")).uid;
        String query = "SELECT * FROM userdata.dashboard WHERE dash_id = ? AND owner = ?";
        Connection conn = DBConnector.connect();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setInt(1, dash_id);
        stmt.setInt(2, owner);
        ResultSet rs = stmt.executeQuery();
        if (!rs.next()) {
            response.sendError(400, "Not your dashboard");
            return;
        }
         query = "INSERT INTO userdata.graphs (graph_id, title, type, owner, rowlimit) " +
             "VALUES ((SELECT MAX(graph_id) FROM userdata.graphs)+1, ?, ?, ?, ?);";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, title);
        stmt.setString(2, type);
        stmt.setInt(3, owner);
        stmt.setInt(4, rowlimit);
        stmt.executeUpdate();
         //DBConnector.doUpdate(query);

         String[] tableArr = tables.split(",");
         String[] columnArr = columns.split(",");
         String[] usageArr = usages.split(",");
         String[] aggregateArr = aggregates.split(",");
         String[] labelArr = labels.split(",");

         for (int i = 0; i < columnArr.length; i++) {
             try {
                 Connection conn2 = DBConnector.connect();
                 PreparedStatement stmt2;

                 if(Objects.equals(aggregateArr[i], "undefined")) {
                     query = "INSERT INTO userdata.columns (graph_id, data_table, data_column, usage, label) " +
                             "VALUES ((SELECT MAX(graph_id) FROM userdata.graphs), ?, ?, ?, ?);";
                     stmt2 = conn2.prepareStatement(query);
                     stmt2.setString(1, tableArr[i]);
                     stmt2.setString(2, columnArr[i]);
                     stmt2.setString(3, usageArr[i]);
                     stmt2.setString(4, labelArr[i]);
                 } else {
                     query = "INSERT INTO userdata.columns (graph_id, data_table, data_column, usage, aggregate, label) " +
                             "VALUES ((SELECT MAX(graph_id) FROM userdata.graphs), ?, ?, ?, ?, ?);";
                     stmt2 = conn2.prepareStatement(query);
                     stmt2.setString(1, tableArr[i]);
                     stmt2.setString(2, columnArr[i]);
                     stmt2.setString(3, usageArr[i]);
                     stmt2.setString(4, aggregateArr[i]);
                     stmt2.setString(5, labelArr[i]);
                 }

                 stmt2.executeUpdate();
             } catch (SQLException e) {
                 throw new RuntimeException(e);
             }

         }

         query = "INSERT INTO userdata.has_graph (dash_id, graph_id) " +
                 "VALUES ( "+ dash_id +", (SELECT MAX(graph_id) FROM userdata.graphs));";
         DBConnector.doUpdate(query);
    }

    @Path("preview")
    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Chart previewChart(@FormParam("title") String title,
                              @FormParam("type") String type,
                              @FormParam("rowlimit") String rowlimit,
                              @FormParam("dash_id") String dash_id,
                              @FormParam("tables") String tables,
                              @FormParam("columns") String columns,
                              @FormParam("usages") String usages,
                              @FormParam("aggregates") String aggregates,
                              @FormParam("labels") String labels) {
         int limit = 0;
         try {
             limit = Integer.parseInt(rowlimit);
         } catch (Exception ignore) {}
         String[] tableArr = tables.split(",");
         String[] columnArr = columns.split(",");
         String[] usageArr = usages.split(",");
         String[] aggregateArr = aggregates.split(",");
         String[] labelArr = labels.split(",");
         return new Chart(-1, title, type, -1, limit, tableArr, columnArr, usageArr, aggregateArr, labelArr);
    }

    @Path("{chartId}")
    public ChartResource getChart (@PathParam("chartId") int id) {return new ChartResource(uriInfo, request, id);}
}
