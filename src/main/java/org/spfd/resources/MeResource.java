package org.spfd.resources;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import org.spfd.DBConnector;
import org.spfd.data.Dashboard;
import org.spfd.data.Session;
import org.spfd.data.User;
import jakarta.xml.bind.Marshaller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

@Path("/me")
public class MeResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public static User me(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        response.setContentType("application/json");
        User user = (User) request.getAttribute("user");
        return user;
    }

    public static User get_user(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }

        for (Cookie cookie: cookies) {
            if (cookie.getName().equals("session")) {
                System.out.println("/api/me: session " + cookie.getValue());
                User user = Session.get_user(cookie.getValue());
//                user.username = escapeHtml4(user.username);
                return user;
            }
        }
        return null;
    }

//    @Path("/dashboards")
//    @GET
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public static List<Dashboard> getDashes(HttpServletRequest request) {
//        Cookie[] cookies = request.getCookies();
//        int uid = 0;
//        for (Cookie cookie: cookies) {
//            if (cookie.getName().equals("session")) {
//                System.out.println("/api/me: session " + cookie.getValue());
//                User user = Session.get_user(cookie.getValue());
//                if(user == null) {
//                    return null;
//                }
//                uid = user.uid;
//            }
//        }
//
//        List<Dashboard> dashboards = new ArrayList<>();
//        String query = "SELECT * " +
//         "FROM userdata.dashboard" +
//         "WHERE dashboard.owner = "+uid+";";
//
//        try (ResultSet rs = DBConnector.doQuery(query)) {
//            while (rs.next()) {
//                int id = rs.getInt("dash_id");
//                String name = rs.getString("name");
//                String icon = rs.getString("icon");
//                dashboards.add(new Dashboard(id, name, icon));
//            }
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//        return dashboards;
//    }
}
