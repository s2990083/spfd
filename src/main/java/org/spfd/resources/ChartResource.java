package org.spfd.resources;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import org.spfd.DBConnector;
import org.spfd.data.Chart;
import org.spfd.data.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChartResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    int id;

    public ChartResource(UriInfo uriInfo, Request request, int id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Chart getChart() throws SQLException {
        String query = "SELECT graphs.graph_id, title, type, owner, rowlimit, array_agg(data_table) AS tables, array_agg(data_column) AS columns, array_agg(USAGE) AS usages, array_agg(AGGREGATE) AS aggregates, array_agg(label) AS labels " +
                "FROM userdata.graphs " +
                "JOIN userdata.columns ON graphs.graph_id = columns.graph_id " +
                "WHERE graphs.graph_id = ?" +
                " GROUP BY graphs.graph_id, title, type, owner, rowlimit; ";
        Connection conn  = DBConnector.connect();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("graph_id");
            String title = rs.getString("title");
            String type = rs.getString("type");
            int owner = rs.getInt("owner");
            int limit = rs.getInt("rowlimit");
            String[] tables = (String[]) rs.getArray("tables").getArray();
            String[] columns = (String[]) rs.getArray("columns").getArray();
            String[] usages = (String[]) rs.getArray("usages").getArray();
            String[] aggregates = (String[]) rs.getArray("aggregates").getArray();
            String[] labels = (String[]) rs.getArray("labels").getArray();
            return new Chart(id, title, type, owner, limit, tables, columns, usages, aggregates, labels);
        }
        return null;
    }

    @DELETE
    public void deleteChart(@Context HttpServletRequest request) throws SQLException {
        int user = ((User) request.getAttribute("user")).uid;
        String query = "DELETE FROM userdata.graphs " +
                "WHERE graph_id = ? AND owner = ?";
        Connection conn = DBConnector.connect();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setInt(1, id);
        stmt.setInt(2, user);
        stmt.executeUpdate();
    }
}
