package org.spfd.resources;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.spfd.DBConnector;
import org.spfd.data.Session;
import org.spfd.data.User;

import java.util.List;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

@Path("users")
public class UsersResource {

    @Path("all")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getAllUsers(@Context HttpServletRequest request, @Context HttpServletResponse response){
        List<User> users = new ArrayList<>();
        response.setContentType("application/json");
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("session")) {
                    System.out.println("has session cookie");
                    User user = Session.get_user(cookie.getValue());
                    assert user != null;
                    if (user.permission_level == 0) {
                        System.out.println("is admin");

                        String query = "SELECT user_id, username, permission_level, setting_theme FROM \"userdata\".\"user\";";
                        try (ResultSet rs = DBConnector.doQuery(query)) {
                            while (rs.next()) {
                                System.out.println("adding user to list");
                                users.add(new User(rs.getInt("user_id"), escapeHtml4(rs.getString("username")), "redacted", rs.getInt("permission_level"), rs.getString("setting_theme")));
                            }
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }

                        System.out.println(users.toString());
                        return users;
                    }
                }
            }
        }

        System.out.println("usr not admin, returning forbidden");
        response.setStatus(403); //todo dis dont work idk why plz fix
        return null;
    }

    private static List<User> getUsers() {
        List<User> users = new ArrayList<>();
        String query = "SELECT user_id, username, permission_level, setting_theme FROM \"userdata\".\"user\";";
        try (ResultSet rs = DBConnector.doQuery(query)) {
            while (rs.next()) {
                System.out.println("adding user to list");
                users.add(new User(rs.getInt("user_id"), rs.getString("username"), "pass", rs.getInt("permission_level"), rs.getString("setting_theme")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("returning users");
        return users;
    }

}
