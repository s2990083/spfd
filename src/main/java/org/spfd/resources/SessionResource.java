package org.spfd.resources;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.JAXBElement;
import org.spfd.data.Credentials;
import org.spfd.data.User;
import org.spfd.DBConnector;
import org.mindrot.jbcrypt.BCrypt;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.NewCookie;
import org.spfd.data.Session;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Path("session")
public class SessionResource {

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("login")
    public void login(@Context HttpServletResponse response, @FormParam("username") String username, @FormParam("password") String password) throws IOException {
        String query = "SELECT user_id, password " +
                "FROM userdata.user " +
                "WHERE username = ?;";

        try {
            Connection conn = DBConnector.connect();
            PreparedStatement stmnt = conn.prepareStatement(query);
            stmnt.setString(1, username);
            ResultSet rs = stmnt.executeQuery();
            if (rs.next()){
                String digest = rs.getString("password");
                if (BCrypt.checkpw(password, digest)) {
                    Session session = new Session(rs.getInt("user_id"));
                    String id = session.save();
                    Cookie cookie = new Cookie("session", id);
                    cookie.setPath("/");
                    System.out.println("Logged " + username +" in: " + cookie);
                    response.addCookie(cookie);
                    response.sendRedirect("../../");
                } else {
                    response.sendRedirect("/login?f=1");
                }
            }
        } catch (SQLException e) {
            response.sendRedirect("/login?f=1");
            throw new RuntimeException(e);
        }
    }

    @DELETE
    @Path("logout")
    public void SessionRemove(@Context HttpServletRequest request, @Context HttpServletResponse response) {

        String sessionid = get_sessionid(request);
        System.out.println("removing session: " + sessionid);

        String query = "DELETE FROM userdata.session WHERE id = ?;";

        try (Connection conn = DBConnector.connect()){
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, sessionid);
            stmt.executeUpdate();
            response.sendRedirect("/account");
        } catch (Exception e) {
            throw new RuntimeException();
        }

    }


    public static String get_sessionid(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie: cookies) {
            if (cookie.getName().equals("session")) {
                System.out.println("/api/me: session " + cookie.getValue());
                String sessionid = cookie.getValue();
                return sessionid;
            }
        }
        return null;
    }

}
