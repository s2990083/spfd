package org.spfd.resources;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import org.spfd.DBConnector;
import org.spfd.data.DBColumn;
import org.spfd.data.DBTable;
import org.spfd.data.Dashboard;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("data")


public class DataResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @Path("tables")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<DBTable> getColumns() {
        List<DBTable> tables = new ArrayList<>();
        String query = "SELECT table_name, array_agg(column_name) AS columns, array_agg(data_type) AS types " +
            "FROM information_schema.columns " +
            "WHERE table_schema = 'ActFact' " +
            "GROUP BY table_name;";
        try (ResultSet rs = DBConnector.doQuery(query)) {
            while(rs.next()) {
                String tableName = rs.getString("table_name");
                String typeString = rs.getString("types");
                String columnString = rs.getString("columns");
                String[] types = convertStringToArray(typeString);
                String[] columnNames = convertStringToArray(columnString);
                List<DBColumn> columns = new ArrayList<>();
                for(int i = 0; i < columnNames.length; i++) {
                    columns.add(new DBColumn(columnNames[i], types[i]));
                }
                tables.add(new DBTable(tableName, columns));
            }
            return tables;
        } catch (SQLException e) {
            throw new RuntimeException(e);
//            return tables;
        }
    }

    public static String[] convertStringToArray(String input) {
        // Remove the curly braces at the beginning and end of the input string
        input = input.substring(1, input.length() - 1);

        // Split the string using comma as the delimiter
        String[] elements = input.split(",");

        // Trim any leading or trailing spaces from each element
        for (int i = 0; i < elements.length; i++) {
            elements[i] = elements[i].trim();
        }

        return elements;
    }
}
