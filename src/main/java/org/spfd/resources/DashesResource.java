package org.spfd.resources;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.UriInfo;
import org.spfd.DBConnector;
import org.spfd.data.Dashboard;
import org.spfd.data.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

@Path("dashboards")
public class DashesResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Dashboard> getDashes(@Context HttpServletRequest request, @QueryParam("owned") boolean owned) throws SQLException {
        int user = ((User) request.getAttribute("user")).uid;
        List<Dashboard> dashboards = new ArrayList<>();
        ResultSet rs;
        if (owned) {
            Connection conn = DBConnector.connect();
            String query = "SELECT * " +
                    "FROM userdata.dashboard WHERE owner=? " +
                     "ORDER BY dash_id ASC;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, user);
            rs = stmt.executeQuery();
        } else {
            String query = "SELECT * " +
                    "FROM userdata.dashboard " +
                     "ORDER BY dash_id ASC;";
            rs = DBConnector.doQuery(query);
        }


        try {
            while (rs.next()) {
                int id = rs.getInt("dash_id");
                String name = rs.getString("name");
                String icon = rs.getString("icon");
                int owner = rs.getInt("owner");
                dashboards.add(new Dashboard(id, name, icon, owner));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dashboards;
    }

        @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public void createDash(@Context HttpServletRequest request, @FormParam("name") String name, @FormParam("icon") String icon) throws SQLException {
        int owner = ((User) request.getAttribute("user")).uid;
        Connection conn = DBConnector.connect();
        String query = "INSERT INTO userdata.dashboard " +
                "VALUES ((SELECT MAX(dash_id) FROM userdata.dashboard)+1, ?, ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setString(1, name);
        stmt.setInt(2, owner);
        stmt.setString(3, icon);
        stmt.executeQuery();
    }

    @Path("{dashId}")
    public DashResource getDash (@PathParam("dashId") int id) {return new DashResource(uriInfo, request, id);}
}
