package org.spfd.data;

import jakarta.xml.bind.annotation.XmlRootElement;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Column {
    public String usage;
    public String label;
    public List<String> data;

    public Column() {}

    public Column(String usage, String label, List<String> data) {
        this.usage = usage;
        this.label = label;
        this.data = data;
    }
}
