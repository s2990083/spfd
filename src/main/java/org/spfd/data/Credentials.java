package org.spfd.data;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Credentials {
    public String username;
    public String password;
}
