package org.spfd.data;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang3.RandomStringUtils;
import org.spfd.DBConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    public int uid;
    public String username;
    public String setting_theme;
    public int permission_level;

    @XmlTransient
    private String password;

    public User() {

    }

    public User(int uid, String username, String password, int permission_level, String setting_theme) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.permission_level = permission_level;
        this.setting_theme = setting_theme;
    }

    public int getUid() {
        return uid;
    }
    public String getUsername() {
        return username;
    }

    public String getSetting_theme() {
        return setting_theme;
    }

    public static int getPermission_level(int uid) {
        String query = "SELECT permission_level "+
                "FROM userdata.user "+
                "WHERE user_id = ?;";
        Connection conn = null;

        try {
            conn = DBConnector.connect();
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, uid);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("permission_level");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return 1;
    }


    public static void saveUser(String username, String password, int permission_level) {
        String query = "insert into userdata.user (username, password, permission_level) " +
                "values(? ,? ,? );";

        Connection conn = null;

        try {
            conn = DBConnector.connect();
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setInt(3, permission_level);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public static void removeUser(int uid) throws RuntimeException {
        String query = "delete from userdata.user " +
                "where user_id = ?;";

        Connection conn = null;

        try {
            conn = DBConnector.connect();
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, uid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void promoteUser(int uid) throws RuntimeException {
        String query = "update userdata.user " +
                "set permission_level = 0 " +
                "where user_id = ?;";

        try {
            Connection conn = DBConnector.connect();
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, uid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void demoteUser(int uid) throws RuntimeException {
        String query = "update userdata.user " +
                "set permission_level = 1 " +
                "where user_id = ?;";

        try {
            Connection conn = DBConnector.connect();
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, uid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
