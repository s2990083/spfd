package org.spfd.data;

import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement
public class DBTable {
    public String tablename;
    public List<DBColumn> columns;

    public DBTable() {}

    public DBTable(String tablename, List<DBColumn> columns) {
        this.tablename = tablename;
        this.columns = columns;
    }
}
