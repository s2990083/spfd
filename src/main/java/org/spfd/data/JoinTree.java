package org.spfd.data;

import org.spfd.DBConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

public class JoinTree {
    public String table;
    public String column;
    public String joinTable;
    public String joinColumn;
    public List<JoinTree> tree;
    public int depth;
    public List<String> remainingTargets;

    public JoinTree(String startingTable, List<String> targets) {
        this.remainingTargets = new ArrayList<>(targets);
        this.remainingTargets = this.remainingTargets.stream()
                                  .filter(target -> !target.equals(startingTable))
                                  .collect(Collectors.toList());
        this.depth = 0;
        this.table = startingTable;
        this.tree = new ArrayList<>();
        if(remainingTargets.size() == 0) {
            return;
        }
        // get a list of all joins
        List<Join> joins = new ArrayList<>();
        String query = "SELECT tc.table_name, " +
                "kcu.column_name, " +
                "ccu.table_name AS foreign_table_name, " +
                "ccu.column_name AS foreign_column_name " +
                "FROM  information_schema.table_constraints tc " +
                "JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name " +
                "JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name " +
                "where tc.constraint_schema='ActFact' " +
                "AND constraint_type = 'FOREIGN KEY';";
        try (ResultSet rs = DBConnector.doQuery(query)) {
            while (rs.next()) {
                String tableName = rs.getString("table_name");
                String columnName = rs.getString("column_name");
                String foreignTableName = rs.getString("foreign_table_name");
                String foreignColumnName = rs.getString("foreign_column_name");
                joins.add(new Join(tableName, columnName, foreignTableName, foreignColumnName));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // check which joins can be applied
        List<Join> availableJoins = new ArrayList<>();
        for(Join join : joins) {
            if (Objects.equals(join.leftTable, this.table)) {
                availableJoins.add(join);
            } else if (Objects.equals(join.rightTable, this.table)) {
                join.invert();
                availableJoins.add(join);
            }
        }
        for(Join join : availableJoins) {
            joins.remove(join);
        }
        // add available joins to the join tree
        for(Join join : availableJoins) {
            this.tree.add(new JoinTree(join.leftTable, join.leftColumn, join.rightTable, join.rightColumn, new ArrayList<>(joins), this.depth, this.remainingTargets));
        }
    }

    public JoinTree(String table, String column, String joinTable, String joinColumn, List<Join> joins, int depth, List<String> targets) {
        this.table = table;
        this.column = column;
        this.joinTable = joinTable;
        this.joinColumn = joinColumn;
        this.tree = new ArrayList<>();
        this.depth = depth + 1;
        this.remainingTargets = new ArrayList<>(targets);
        this.remainingTargets = this.remainingTargets.stream()
                          .filter(target -> !target.equals(joinTable))
                          .collect(Collectors.toList());
        if(remainingTargets.size() == 0) {
            return;
        }
        // check which joins can be applied
        List<Join> availableJoins = new ArrayList<>();
        for(Join join : joins) {
            if (Objects.equals(join.leftTable, this.joinTable)) {
                availableJoins.add(join);
            } else if (Objects.equals(join.rightTable, this.joinTable)) {
                join.invert();
                availableJoins.add(join);
            }
        }
        for(Join join : availableJoins) {
            joins.remove(join);
        }
        // add available joins to the join tree
        for(Join join : availableJoins) {
            this.tree.add(new JoinTree(join.leftTable, join.leftColumn, join.rightTable, join.rightColumn, new ArrayList<>(joins), this.depth, remainingTargets));
        }
    }

    public void removeIncompleteBranches() {
        List<JoinTree> incompleteBranches = new ArrayList<>();
        for(JoinTree branch : this.tree) {
            if(branch.remainingTargets.size() > 0 && branch.tree.size() > 0) {
                branch.removeIncompleteBranches();
            }
            if(branch.remainingTargets.size() > 0 && branch.tree.size() == 0) {
                incompleteBranches.add(branch);
            }
        }
        for(JoinTree branch : incompleteBranches) {
            this.tree.remove(branch);
        }
    }

    public int getMinBranchLength() {
        if (this.tree.size() == 0 ) {
            return 0;
        }
        List<Integer> length = this.tree.stream()
                              .map(JoinTree::getMinBranchLength)
                              .toList();
        return Collections.min(length) + 1;
    }

    public void removeLongerBranches() {
        Optional<JoinTree> leaf = this.tree.stream().filter(branch -> branch.tree.size() == 0).findFirst();
        if (leaf.isPresent()) {
            this.tree = this.tree.stream().filter(branch -> branch.equals(leaf.get())).collect(Collectors.toList());
            return;
        }
        // take branch with minimum branch length
        List<Integer> lengths = this.tree.stream()
                      .map(JoinTree::getMinBranchLength)
                      .toList();
        int minLength = Collections.min(lengths);
        this.tree = this.tree.stream().filter(branch -> branch.getMinBranchLength() == minLength).collect(Collectors.toList());
        this.tree = this.tree.subList(0,1);
        this.tree.get(0).removeLongerBranches();
    }

    public String stringify() {
        StringBuilder sb = new StringBuilder();
        for(JoinTree branch : this.tree) {
            sb.append("JOIN \"ActFact\".\"")
              .append(branch.joinTable)
              .append("\" ON \"")
              .append(branch.table)
              .append("\".\"")
              .append(branch.column)
              .append("\" = \"")
              .append(branch.joinTable)
              .append("\".\"")
              .append(branch.joinColumn)
              .append("\" ");
            sb.append(branch.stringify());
        }
        return sb.toString();
    }

    public String toString() {
        return "table: " + this.table + "\n" +
                "column: " + this.column + "\n" +
                "joinTable: " + this.joinTable + "\n" +
                "joinColumn: " + this.joinColumn + "\n" +
                "depth: " + this.depth + "\n" +
                "remainingTargets: " + this.remainingTargets + "\n" +
                "next: " + this.tree.toString() + "\n";
    }

    public static class Join {
        public String leftTable;
        public String leftColumn;
        public String rightTable;
        public String rightColumn;

        public Join(String leftTable, String leftColumn, String rightTable, String rightColumn) {
            this.leftTable = leftTable;
            this.leftColumn = leftColumn;
            this.rightTable = rightTable;
            this.rightColumn = rightColumn;
        }

        public void invert() {
            String table = this.leftTable;
            String column = this.leftColumn;
            this.leftTable = this.rightTable;
            this.leftColumn = this.rightColumn;
            this.rightTable = table;
            this.rightColumn = column;
        }

        public String toString() {
            return "(" + leftTable + ") JOIN " + rightTable + " ON " + leftColumn + " = " + rightColumn + ";";
        }
    }
}