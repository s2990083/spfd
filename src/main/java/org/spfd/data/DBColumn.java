package org.spfd.data;

import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement
public class DBColumn {
    public String columnName;
    public String type;

    public DBColumn() {}

    public DBColumn(String columnName, String type) {
        this.columnName = columnName;
        this.type = type;
    }
}
