package org.spfd.data;

import org.spfd.DBConnector;

import java.sql.*;

import org.apache.commons.lang3.RandomStringUtils;

public class Session {
    public int user;
    public String expiry;

    public Session(int user) {
        this.user = user;
    }

    public String save() {
        String randomstring = RandomStringUtils.randomAlphabetic(10);
        String query = "INSERT INTO userdata.session (id, \"uid\", expiry) " +
                "VALUES (?, '" + this.user + "', now() + interval '1 month')";
        try {
            Connection conn = DBConnector.connect();
            PreparedStatement stmnt = conn.prepareStatement(query);
            stmnt.setString(1, randomstring);
            stmnt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return randomstring;
    }

    public static User get_user(String id) {
        String query = "SELECT user_id, username, password, permission_level, setting_theme FROM \"userdata\".\"session\" s, \"userdata\".\"user\" u WHERE s.id = '" + id + "' AND s.uid = u.user_id AND now() < expiry LIMIT 1;";
        ResultSet rs = DBConnector.doQuery(query);
        try {
            rs.next();
            return new User(rs.getInt("user_id"), rs.getString("username"), rs.getString("password"), rs.getInt("permission_level"), rs.getString("setting_theme"));
        } catch (SQLException e) {
            return null;
        }

    }


}
