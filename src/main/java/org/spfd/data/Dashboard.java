package org.spfd.data;

import jakarta.xml.bind.annotation.XmlRootElement;
import org.spfd.DBConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Dashboard {
    public int id;
    public String name;
    public String icon;
    public List<Chart> charts;
    public int owner;

    public Dashboard() {
    }

    public Dashboard(int id, String name, String icon, Object[] graphIds, int dashOwner) throws SQLException {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.owner = dashOwner;
        this.charts = new ArrayList<>();
        String query = "SELECT graphs.graph_id, title, type, owner, rowlimit, array_agg(data_table) AS tables, array_agg(data_column) AS columns, array_agg(USAGE) AS usages, array_agg(AGGREGATE) AS aggregates, array_agg(label) AS labels " +
                "FROM userdata.has_graph " +
                "JOIN userdata.graphs ON has_graph.graph_id = graphs.graph_id " +
                "JOIN userdata.columns ON graphs.graph_id = columns.graph_id " +
                "WHERE dash_id = ?" +
                " GROUP BY graphs.graph_id, title, type, owner, rowlimit " +
                "ORDER BY graphs.graph_id ASC; ";
        Connection conn = DBConnector.connect();
        PreparedStatement stmt = conn.prepareStatement(query);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int graph_id = rs.getInt("graph_id");
            String title = rs.getString("title");
            String type = rs.getString("type");
            int owner = rs.getInt("owner");
            int limit = rs.getInt("rowlimit");
            String[] tables = (String[]) rs.getArray("tables").getArray();
            String[] columns = (String[]) rs.getArray("columns").getArray();
            String[] usages = (String[]) rs.getArray("usages").getArray();
            String[] aggregates = (String[]) rs.getArray("aggregates").getArray();
            String[] labels = (String[]) rs.getArray("labels").getArray();
            charts.add(new Chart(graph_id, title, type, owner, limit, tables, columns, usages, aggregates, labels));
        }
    }

        public Dashboard(int id, String name, String icon, int owner) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.owner = owner;
    }

    public String getName() {
        return name;
    }
}
