package org.spfd.data;

import jakarta.xml.bind.annotation.XmlRootElement;
import org.spfd.DBConnector;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@XmlRootElement
public class Chart {
    public int id;
    public String title;
    public String type;
    public int owner;
    public List<Column> columns;

    public Chart() {
    }

    public Chart(int id, String title, String type, int owner, int limit, String[] tables, String[] columns, String[] usages, String[] aggregates, String[] labels) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.owner = owner;
        this.columns = new ArrayList<>();

        // building the query
        StringBuilder query = new StringBuilder("SELECT ");
        for (int i = 0; i < columns.length; i++) {
            if (i > 0) {
                query.append(", ");
            }
            if (aggregates[i] != null && !Objects.equals(aggregates[i], "null") && !Objects.equals(aggregates[i], "undefined")) {
                query.append(aggregates[i]).append("(\"").append(tables[i]).append("\".\"").append(columns[i]).append("\") AS ").append(tables[i]).append("_").append(aggregates[i]).append("_").append(columns[i]);
            } else {
                query.append("\"").append(tables[i]).append("\".\"").append(columns[i]).append("\" AS ").append(tables[i]).append("_").append(aggregates[i]).append("_").append(columns[i]);
            }
        }
        query.append(" FROM \"ActFact\".\"").append(tables[0]).append("\" ");

        List<String> uniqueTables = Arrays.stream(tables).distinct().toList();
        if(uniqueTables.size() > 1) {
            JoinTree joinTree = new JoinTree(tables[0], uniqueTables);
            joinTree.removeIncompleteBranches();
            joinTree.removeLongerBranches();
            query.append(joinTree.stringify());
        }

        query.append("GROUP BY ");
        boolean addedGrpBy = false;
        for (int i = 0; i < columns.length; i++) {
            if (aggregates[i] == null || Objects.equals(aggregates[i], "null") || Objects.equals(aggregates[i], "undefined")) {
                if (addedGrpBy) {
                    query.append(", ");
                }
                query.append(tables[i]).append("_").append(aggregates[i]).append("_").append(columns[i]);
                addedGrpBy = true;
            }
        }
        query.append(" ORDER BY ");

        boolean addedAgg = false;
        for (int i = 0; i < columns.length; i++) {
            if (aggregates[i] == null || Objects.equals(aggregates[i], "null") || Objects.equals(aggregates[i], "undefined")) {
                if (addedAgg) {
                    query.append(", ");
                }
                query.append(tables[i]).append("_").append(aggregates[i]).append("_").append(columns[i]);
                addedAgg = true;
            }
        }
        if (limit != 0) {
            query.append(" LIMIT ").append(limit);
        }
        query.append(";");

        System.out.println(query.toString());

        // executing the query
        List<List<String>> data = new ArrayList<>();
        for (int i = 0; i < columns.length; i++) {
            data.add(new ArrayList<>());
        }

        System.out.println(query);

        try (ResultSet rs = DBConnector.doQuery(query.toString())) {
            while (rs.next()) {
                for (int i = 0; i < columns.length; i++) {
                    data.get(i).add(rs.getString(tables[i]+"_"+aggregates[i]+"_"+columns[i]));
                }
            }
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < columns.length; i++) {
            this.columns.add(new Column(usages[i], labels[i], data.get(i)));
        }
    }

    public Chart(int id, String title, String type, int owner) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.owner = owner;
        this.columns = null;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public int getOwner() {
        return owner;
    }

    public List<Column> getColumns() {
        return columns;
    }
}

