# ActFact 1

This is an university project on behalf of the company ActFact.

### File Structure
* .idea folder - Contains .xml files and the artifacts.
* src/main/webapp - Contains the JavaScript, CSS and HTML related to the web application. Every web page has its own folder.
* src/main/java/org/spfd/data - Contains all the .java files related to the storing of data. For example the data regarding the dashboards.
* src/main/java/org/spfd/resources - Contains all the 'resource(s)' classes for the functionality of the web application.

### Starting the application
* Open project in preferred IDE.
* Add TomCat server, with the .war file found in .idea/artifacts.
* Run project.
* login with the following credentials:

  - username: Username

  - password: strongPassword

* Enjoy!
